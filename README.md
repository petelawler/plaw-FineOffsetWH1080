FineOffsetWH1080
=========

An more improved Arduino sketch for decoding packets from the outdoor temperature and humidity sensor from Fine Offset Electronics as sold in Australia by Jaycar Electronics as the WH1080.

* Original code Created by [Luc Small](https://github.com/lucsmall/BetterWH2) on 19 July 2013 and released into released into the public domain.
* Updated for WH1080 by Mike Severs 2015-2016
* Delivered to Peter Lawler at Hobart Makers in 2017 who then diffed against original repo, imported and applied the patch and removed a bit of whitespace.

***NOTE:*** You’ll have to modify the value of valid_sensorID to suit your system.  The weather station assigns a new station ID when you restart it, eg after changing its battery. The hhead unit (touchscreen) sends a pulse every 48 seconds to the weather station, and this code receives the reply.

This code contains a CRC-8 function adapted from the [Arduino OneWire library](http://www.pjrc.com/teensy/td_libs_OneWire.html). Thanks go to the authors of that project.
